---
layout: page
title: Downloads
date: 2012-01-22 15:52:17
permalink: /downloads/
---
# PC Games (old)

<p>Be aware that these applications are not finished, they will not break your computer, but you may have to restart it.</p>
<p>You can also download the sources of the applications on <a title="Projects" href="http://blog.mobiledev.pl/produkcje/">Projects</a> page.</p>
<table border="0">
<tbody>
<tr>
<td><a href="http://blog.mobiledev.pl/wp-content/uploads/2012/01/CoolTetris.png"><img class="size-thumbnail wp-image-728 alignleft" title="CoolTetris" alt="" src="/assets/CoolTetris-e1327247086452-150x118.png" width="150" height="118" /></a><br />
<strong>CoolTetris</strong> is my first game written in DirectDraw back in 2001(?).<br />
<a href="http://blog.mobiledev.pl/wp-content/uploads/2012/01/CoolTetris.zip">Download</a></td>
</tr>
<tr>
<td><br /><a href="http://blog.mobiledev.pl/wp-content/uploads/2012/01/Zawaditier.png"><img class="alignleft  wp-image-733" title="Zawaditier" alt="" src="/assets/Zawaditier-150x150.png" width="150" height="150" /></a><br />
<strong>Zawaditier</strong> is simple racing game.<br />
<a href="http://blog.mobiledev.pl/wp-content/uploads/2012/01/Zawaditier.zip">Download</a></td>
</tr>
<tr>
<td><br /><a href="http://blog.mobiledev.pl/wp-content/uploads/2012/01/LiquidMastahEngine.jpg"><img class="alignleft size-thumbnail wp-image-731" title="LiquidMastahEngine" alt="" src="/assets/LiquidMastahEngine-150x150.jpg" width="150" height="150" /></a><br />
<strong>LiquidMastahEngine</strong> was my first attempt to create simple 3d outdoor engine.<br />
<a href="http://blog.mobiledev.pl/wp-content/uploads/2012/01/LiquidMastahEngine.zip">Download</a></td>
</tr>
<tr>
<td><br /><a href="http://blog.mobiledev.pl/wp-content/uploads/2012/01/Hunter.png"><img class="alignleft size-thumbnail wp-image-729" title="Hunter" alt="" src="/assets/Hunter-e1327247318532-150x120.png" width="150" height="120" /></a><br />
<strong>Hunter</strong> was meant to be 2d rpg game. What has been implemented is just a simple map editor.<br />
<a href="http://blog.mobiledev.pl/wp-content/uploads/2012/01/Hunter.zip">Download</a></td>
</tr>
<tr>
<td><br /><a href="http://blog.mobiledev.pl/wp-content/uploads/2012/01/Sokoban3D.png"><img class="alignleft size-thumbnail wp-image-732" title="Sokoban3D" alt="" src="/assets/Sokoban3D-150x150.png" width="150" height="150" /></a><br />
<strong>Sokoban3D</strong>.<br />
<a href="http://blog.mobiledev.pl/wp-content/uploads/2012/01/Sokoban3D.zip">Download</a></td>
</tr>
<tr>
<td><br /><a href="http://blog.mobiledev.pl/wp-content/uploads/2012/01/ZulsGame.png"><img class="alignleft size-thumbnail wp-image-734" title="ZulsGame" alt="" src="/assets/ZulsGame-150x150.png" width="150" height="150" /></a><br />
<strong>ZUL's game</strong>was inspired by StarCraft's well balanced gameplay, and it was attempt to bring this balance to 2D shooter.<br />
<a href="http://blog.mobiledev.pl/wp-content/uploads/2012/01/ZULs-Game.zip">Download</a></td>
</tr>
<tr>
<td></td>
</tr>
<tr>
<td></td>
</tr>
<tr>
<td></td>
</tr>
<tr>
<td></td>
</tr>
<tr>
<td></td>
</tr>
<tr>
<td></td>
</tr>
</tbody>
</table>
