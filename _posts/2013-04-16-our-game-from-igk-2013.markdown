---
layout: post
title: Game from IGK 2013
date: 2013-04-16 21:52:59
---
Last weekend(5-7 April) I have been on game development conference IGK in Siedlce. It was 10th anniversary of the conference, so even that I had to take morning flight, and then another one to be back at work on Monday morning, it was worth it.

I can't say much about presentation that took place during the conference, as I came at the end of second day, but I have started with my team (Kamil "ayufan" Trzciński, Konrad "Rodrigo" Rodzik, Jarek "agent_j" Pelczar) in 8h game programming contest "Compo" and it was awesome!

![img](/assets/Capture2_tanks.png)

This year, twelve teams joined the competition, producing in 8h all kinds of crazy games. Our game took 2nd place, we made top-down "tank vs tank" with multiplayer up to 4 players, with tanks controlled from a smartphone. The source code can be downloaded from bitbucket: [https://bitbucket.org/gosuwachu/igk-2013/src](https://bitbucket.org/gosuwachu/igk-2013/src).

As always time was short, but we managed to do everything we planned and even a little bit more. Game was written in C++ and cocos2d-x.
I just want to congratulate to all the teams that started this year, and I hope that we will meet again next year!
<p><center><br />
<iframe src="http://www.youtube.com/embed/Fi2tqghZDOU" height="315" width="420" allowfullscreen="" frameborder="0"></iframe></center></p>
