---
layout: post
title: Managing your GMail account
date: 2012-01-28 15:20:51
---
![img](http://blog.mobiledev.pl/wp-content/uploads/2012/01/images1.jpg)
<p>GMail is popular email service provided by Google. Whether it is your professional or private email, in both cases, your inbox can get filled with emails about different subjects, with some important more than others. Here I will tell you about few things that I have learnt about managing my inbox, and how to stay productive.</p>
<p><!--more--></p>
<p><strong>Keep your inbox small</strong></p>
<p>When I first started using GMail, I used to keep all my emails in inbox. I quickly figured, that sometimes when I get an email I should do something about it, like pay my bills. Keeping all the emails in inbox makes your life harder, as you don't know which of things you have already done, and which not. Solution to this problem is simple: if you have an email that requires some action from you, leave it in inbox until it is finished, after that just archive it. This way you can postpone tasks, and never forget about them. You will also know what have already been done.</p>
<p><strong>Do not delete emails, archive them</strong></p>
<p>Keeping track of all of your emails is important. You never know when you will need some information again. Obvious example is when you need that login and password to some service that you get when you first register there, but you may also want to know if you if someone is waiting for you to reply, or to prove that you have done what you have been asked for. It is especially important when working with clients. Send email after a meeting, summarizing everything you came up with, and ask people to verify, if everything is correct. If after few months someone will get back to you, saying that you haven't done something, you will have that email. It works, and we use it a lot.</p>
<p><strong>Use shortcuts</strong></p>
<p>You just came to work and already have 50 new messages? An hour for digging through wasted? No. Learn <a title="GMail keyboard shortcuts" href="http://support.google.com/mail/bin/answer.py?hl=en&amp;answer=6594">keyboard shortcuts</a> for the most frequently used actions. My favorites are:</p>
<p>[, ] - archive and move to previous/next message<br />
j, k - move to previous/next message<br />
c - compose new message</p>
<p>Small thing, but will save you a lot of time in the end.</p>
<p><strong>Search</strong></p>
<p>Google has great search engine, which also works for your emails. Instead of going through list of 5,000 messages, try to use search. You can use advanced search options to find things easier, here is the list of all the options: <a title="GMail advanced search" href="http://support.google.com/mail/bin/answer.py?hl=en&amp;answer=7190">http://support.google.com/mail/bin/answer.py?hl=en&amp;answer=7190</a>.</p>
