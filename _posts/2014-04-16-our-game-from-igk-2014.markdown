---
layout: post
title: "Game from IGK 2014"
date: 2014-04-16 11:49:09
comments: true
---
My team have just came back from IGK 2014 game developer conference. Below is a screenshot from our game, and link if you would like to play it: [IGK2014](http://blog.mobiledev.pl/wp-content/uploads/2014/04/IGK2014.zip)

![screenshot](http://blog.mobiledev.pl/wp-content/uploads/2014/04/igk20141.jpg)
