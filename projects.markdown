---
layout: page
title: Projects
permalink: /projects/
---
### Windows Phone apps

[Bluescreen](http://www.windowsphone.com/en-US/apps/cce6234a-0c28-e011-854c-00237de2db9e)

[Bord](http://www.windowsphone.com/en-US/apps/e6ce0700-3f8a-e011-986b-78e7d1fa76f8)

[Chuck Norris Jokes](http://www.windowsphone.com/en-US/apps/8bb06e73-75b7-41a7-9846-77548bbc82be)

[Pocket Translator](http://www.windowsphone.com/en-US/apps/d1057ce1-e207-e011-9264-00237de2db9e)

[Todosit](http://www.windowsphone.com/en-US/apps/c0af786c-0780-e011-986b-78e7d1fa76f8)

[TranslatorPro](http://www.windowsphone.com/en-US/apps/7da5ac30-5f74-4af4-ac33-d6177317eec5)

